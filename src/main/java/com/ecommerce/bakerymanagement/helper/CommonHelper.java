package com.ecommerce.bakerymanagement.helper;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.ecommerce.bakerymanagement.configuration.SpringS3AwsConfig;

@Component
public class CommonHelper {

	private SpringS3AwsConfig s3AwsConfig;

	public String getS3DownloadUrl(Map<String, String> bucketDetails) {
		StringBuilder urlBuilder = new StringBuilder(s3AwsConfig.getS3Url());
		urlBuilder.append("/");
		urlBuilder.append(bucketDetails.get("bucketName"));
		urlBuilder.append("/");
		urlBuilder.append(bucketDetails.get("folderName"));
		urlBuilder.append("/");
		urlBuilder.append(bucketDetails.get("fileName"));

		return String.valueOf(urlBuilder);
	}
}
