package com.ecommerce.bakerymanagement.biz.client.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.ecommerce.bakerymanagement.aws.client.IS3AwsConfigurationClient;
import com.ecommerce.bakerymanagement.biz.client.IAdminConfigurationClient;

import lombok.extern.log4j.Log4j;

@Component
@Log4j
public class AdminConfigurationClientImpl implements IAdminConfigurationClient {
	
	private IS3AwsConfigurationClient s3AwsConfigurationClient;

	@Override
	public Map<String, String> saveAdminDocuments(List<MultipartFile> multipartFiles,Map<String, String> bucketDetails) {
		
		
		
		for (MultipartFile multipartFile : multipartFiles) {
			s3AwsConfigurationClient.s3Uplod(multipartFile, bucketDetails);
		}
		
		return null;
	}

}
