package com.ecommerce.bakerymanagement.biz.client;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public interface IAdminConfigurationClient {

	Map<String, String> saveAdminDocuments(List<MultipartFile> multipartFiles, Map<String, String> bucketDetails);

}
