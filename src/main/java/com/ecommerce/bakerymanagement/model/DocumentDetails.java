package com.ecommerce.bakerymanagement.model;

import java.sql.Timestamp;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection="Bakery_manangement.document_details")
@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentDetails {
	
	@Field(value="document_id")
	private String documentId;
	
	@Field(value="document_name")
	private String documentName;
	
	@Field(value="s3_link")
	private String s3Link;
	
	@Field(value="document_type")
	private String documentType;
	
	@Field(value="created_date")
	private Timestamp  createdDate;
	
	@Field(value="last_modified_date")
	private Timestamp lastModifiedDate;
	
	@Field(value="created_user_id")
	private Long createdUserId;
	
	@Field("last_modified_user_id")
	private Long lastModifiedUserId;
	

}
