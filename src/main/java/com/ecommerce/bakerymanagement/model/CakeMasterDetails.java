package com.ecommerce.bakerymanagement.model;

import java.sql.Timestamp;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * It is cakeMasterDetails Bean Class.
 * 
 * @author Danish
 *
 */

@Document(collection = "Bakery_manangement.cake_master")
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CakeMasterDetails {

	@Id
	@Field("cake_id")
	private String cakeId;

	@Size(min = 3, max = 40,message="min or max cake name limit exceed")
	@NotNull(message="cakename ")
	@Field("cake_name")
	private String cakeName;

	@Size(min = 8, max = 40)
	@NotNull
	@Field("flavour")
	private String flavour;

	@Size(min = 4, max = 20)
	@NotNull
	private String category;

	@Min(200)
	@Max(1000)
	@Indexed(direction = IndexDirection.ASCENDING)
	private Double price;

	@NotNull
	private Boolean availability;

	@Min(1)
	@Max(4)
	@NotNull
	private String weight;

	@JsonFormat(pattern = "MMM dd, yyyy hh:mm:ss aa")

	@Field("created_date")
	private Timestamp createdDate;

	@JsonFormat(pattern = "MMM dd, yyyy hh:mm:ss aa")
	@Field("last_modified_date")
	private Timestamp lastModifiedDate;

	@Field("created_user_id")

	@CreatedBy
	private Long createdUserId = 1L;

	@Field("last_modified_user_id")

	@LastModifiedBy
	private Long lastModifiedUserId = 1L;

}
