package com.ecommerce.bakerymanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ecommerce.bakerymanagement.constants.WebConstants;

import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j;

@RequestMapping(value = "/admin/v1")
@RestController
@Api(value = "onlinecakestore", description = "Admin Operations pertaining to products in Online Store")
@Log4j
public class AdminConfigurationController {
	
	

	@PostMapping(value = WebConstants.SAVE_ADMIN_DOCUMENTS)
	private @ResponseBody Object saveNewDocuments(List<MultipartFile> multipartFiles,
			HttpServletResponse httpServletResponse) {
		try {
			
		} catch (Exception e) {
			log.info("", e);
		}
		return null;
	}
	

}
